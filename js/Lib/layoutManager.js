
/*
 * Manager for layout which configure what and where to present widget
 * */
var layoutManager = new function () {
}

/*
 * check is andriod device
 * */
layoutManager.setUpAppearance = function () {
    var widgetJSONCollection = [];
    widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('logo', 'div.widgetRow:nth-child(1) > div:nth-child(1)'));
    widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('welcome', 'div.widgetRow:nth-child(1) > div:nth-child(2)'));
    widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('search', 'div.widgetRow:nth-child(1) > div:nth-child(3)'));
    widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('tags', 'div.widgetRow:nth-child(2) > div:nth-child(1)'));
    widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('news', 'div.widgetRow:nth-child(3) > div:nth-child(1)'));
    widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('recommendedStories', 'div.widgetRow:nth-child(4) > div:nth-child(1)'));
    widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('recommendedFiles', 'div.widgetRow:nth-child(4) > div:nth-child(2)'));
    widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('feeds', 'div.widgetRow:nth-child(4) > div:nth-child(3)'));

    widgetManager.setUpAppearance(widgetJSONCollection);
}

