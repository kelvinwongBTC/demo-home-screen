
/*
 * helper object for device
 * */
var deviceHelper = new function () {
}

/*
* check is andriod device
* */
deviceHelper.isAndroidDevice = function () {
    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1;

    return isAndroid;
}
