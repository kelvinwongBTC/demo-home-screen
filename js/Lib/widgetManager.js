/*
 * Widget manager
 *
 * provide dynamic content
 * */
var widgetManager = new function () {
}

/* get widget path by name
*
* @para widgetName
* */
widgetManager.widgetPathByName = function (widgetName) {
    return this.widgetHost(widgetName) + '/index.html';
}

/* get widget css path by name
 *
 * @para widgetName
 * */
widgetManager.widgetCSSPathByName = function (widgetName) {
    return this.widgetHost(widgetName) + '/style.css';
}


/* get widget host
 *
 * @para widgetName
 * */
widgetManager.widgetHost = function (widgetName) {
    return document.URL.replace('index.html', '') + 'widget/'+widgetName;
}

/*
* get url content
*
* @para url
* @para callback (data, error)
* */
widgetManager.getContentWithURL = function (url, callback) {
    // Assign handlers immediately after making the request,
// and remember the jqxhr object for this request
    console.log('try to get widget from path :'+url);

    var jqxhr = $.get( url, function(data) {
        if(callback)
            callback(data, null);
    })
    .done(function(data) {
    })
    .fail(function(xhr, textStatus, errorThrown) {
        console.log( "error" );

            if(callback)
                callback(null, errorThrown);
    })
    .always(function(data) {
    });
}


/*
* get content from url and append into the target dom object
*
* @para url
* @para target dom object
* @para callback(error)
* */
widgetManager.appendContentIntojQueryElementObject = function(url, object, callback) {
    this.getContentWithURL(url, function(data, error) {
        if (error !== null) {
           console.log('error :' + error);
            if (callback) callback(error);
            return;
        }

        // append data
        object.html(data);
        if (callback) callback(null);
    });
}


var jQueryObjectKey = "jQeuryObjectKey"
var widgetKey = "widgetKey";

/*
* json object by url and jQueryObject (html element)
*
* @para widgetName
* @para jQuerySelector
*
* @return json object
* */
widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey = function (widgetName, jQuerySelector) {

    if (widgetName === null || jQuerySelector === null) {
        console.log('please identify the required element');
        return null;
    }

    var jsonObj = {
        widgetKey : widgetName,
        jQueryObjectKey: jQuerySelector
    };

    return jsonObj;
}


/*
* set up for layout appearance
*
* @para array of json object
 * - object : { url, css, jQueryObj }
* */
widgetManager.setUpAppearance = function (collection) {

    collection.forEach(function(object) {

        var widget = object.widgetKey;
        var jObj = object.jQueryObjectKey;

        if (object === null || url === null || jObj === null) {
            console.log('please identify the required element');
           return;
        }

        var url = widgetManager.widgetPathByName(widget);
        var css = widgetManager.widgetCSSPathByName(widget);

        widgetManager.appendContentIntojQueryElementObject(url,$(jObj), function(error){
            if(error === null) {
                widgetManager.loadCssWithCssPath(css, widget);
            }
        });

    });

}

/*
* load css file by path and name
*
* @para path
* @para widgetName
* */
widgetManager.loadCssWithCssPath = function(path, widgetName) {

    var $ = document; // shortcut
    var cssId = widgetName;
    if (!$.getElementById(cssId))
    {
        var head  = $.getElementsByTagName('head')[0];
        var link  = $.createElement('link');
        link.id   = cssId;
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = path;
        link.media = 'all';
        head.appendChild(link);
    }
}





