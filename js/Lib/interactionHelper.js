// enum for coordinateType
var coordinateType = {
    'X' : 0,
    'Y' : 1
}
Object.freeze(coordinateType);

/*
 * helper object for interaction
 * */
var interactionHelper = new function () {
}


/**
 * add Touch Event with custome callbacks including touchstart and touchend
 *
 * @para jQObj (jQuery Object)
 * @para onTouchStartCallback(jQObj, event)
 * @para onTouchMoveCallback(jQObj, event, isSwiping, isScrolling)
 * @para onTouchEndCallback(jQObj, event)
 * @para onTapCallback(jQObj, event)
 */
interactionHelper.addTouchEventWithCustomCallbacks = function (jQObj, onTouchStartCallback, onTouchMoveCallback, onTouchEndCallback, onTapCallback) {
    // source : http://blog.mobiscroll.com/working-with-touch-events/
    var touch,
        action,
        diffX,
        diffY,
        endX,
        endY,
        scroll,
        sort,
        startX,
        startY,
        swipe,
        sortTimer;

    function testTouch(e) {
        if (e.type == 'touchstart') {
            touch = true;
        } else if (touch) {
            touch = false;
            return false;
        }
        return true;
    }

    function onStart(ev) {
        if (testTouch(ev) && !action) {

            action = true;

            startX = getCoord(ev, 'X');
            startY = getCoord(ev, 'Y');
            diffX = 0;
            diffY = 0;

            if (deviceHelper.isAndroidDevice()) {
                sortTimer = setTimeout(function () {
                    sort = true;
                }, 200);
            }

            if (ev.type == 'mousedown') {
                $(document).on('mousemove', onMove).on('mouseup', onEnd);
            }

            if (onTouchStartCallback) {
                onTouchStartCallback(jQObj, ev) ;
            }
        }
    }

    function onMove(ev) {
        if (action) {

            endX = getCoord(ev, 'X');
            endY = getCoord(ev, 'Y');
            diffX = endX - startX;
            diffY = endY - startY;

            if (Math.abs(diffY) > 10) { // It's a scroll
                scroll = true;

                // Android 4.0 will not fire touchend event
                if (deviceHelper.isAndroidDevice()) {
                    $(this).trigger('touchend');
                }
            } else if (Math.abs(diffX) > 7) { // It's a swipe
                swipe = true;
            }

            if (swipe) {
                ev.preventDefault(); // Kill page scroll
                // Handle swipe
            }

            if (sort) {
                ev.preventDefault(); // Kill page scroll
                // Handle sort
            }

            if (Math.abs(diffX) > 5 || Math.abs(diffY) > 5) {
                clearTimeout(sortTimer);
            }

            if (onTouchMoveCallback) {
                onTouchMoveCallback(jQObj, ev, swipe, scroll) ;
            }
        }
    }

    function onEnd(ev) {
        if (action) {

            action = false;

            var didInvokeTapEvent = false;

            if (swipe) {
                // Handle swipe end

            } else if (sort) {
                // Handle sort end

            } else if (!scroll && Math.abs(diffX) < 5 && Math.abs(diffY) < 5) { // Tap
                if (ev.type === 'touchend') { // Prevent phantom Taps
                    ev.preventDefault();
                }
                // Handle tap
                didInvokeTapEvent = true;
                if (onTapCallback) {
                    onTapCallback(jQObj, ev) ;
                }
            }

            swipe = false;
            sort = false;
            scroll = false;

            clearTimeout(sortTimer);

            if (ev.type == 'mouseup') {
                $(document).off('mousemove', onMove).off('mouseup', onEnd);
            }

            if (!didInvokeTapEvent && onTouchEndCallback) {
                onTouchEndCallback(jQObj, ev) ;
            }

        }
    }

    /*
     * get Coordinates
     * */
    function getCoord(e, c) {
        if (c === 'X') {
            return interactionHelper.getCoordinate(e, coordinateType.X);
        }

        if (c === 'Y') {
            return interactionHelper.getCoordinate(e, coordinateType.Y);
        }

        return null;
    }

    jQObj.on('touchstart mousedown', onStart)
        .on('touchmove', onMove)
        .on('touchend touchcancel', onEnd)
}

/*
 * add Touch event helper for default action
 * */
interactionHelper.addTouchEvent = function (jQObj, callback) {
    var didItemMove; // as long as item has been moved, it should be un-highlighted and not firing callback

    /*
     * default Touch Start method
     * */
    function handleTouchStartEvent (obj, event) {
         obj.addClass('highightedItem');

        // init var
        didItemMove = false;

        interactionHelper.standardTouchStartEvent(obj, event);
    }

    function handleTouchMoveEvent(obj, event, isSwiping, isScrolling) {
        // if item has been moved
        if (interactionHelper.hasItemMovedWithEvent(obj, event, isSwiping, isScrolling)) {
            didItemMove = true;
            obj.removeClass('highightedItem');
        }
    }

    /*
     * default TouchEnd method
     * */
    function handleTouchEndEvent(obj, event) {
        obj.removeClass('highightedItem');

        if (!didItemMove) {
            interactionHelper.standardTouchEndEvent(obj, event, callback);
        }
    }

    function handleTapEvent(obj, event) {
        obj.removeClass('highightedItem');

        // for fixing iOS mobile safari alert problem, we need to have timeout
        // when any alert is shown the touch end is never get fired
        setTimeout(function(){
            callback()
        }, 0);
    }

    interactionHelper.addTouchEventWithCustomCallbacks(jQObj, handleTouchStartEvent, handleTouchMoveEvent, handleTouchEndEvent, handleTapEvent);
}

/*
* Standard Touch Event (should be commonly used)
* */
interactionHelper.standardTouchStartEvent = function (jQObj, event) {
    // add start time data
    jQObj.data('startTime', new Date());

    jQObj.data('originalX', jQObj.offset().left - $(window).scrollLeft());
    jQObj.data('originalY', jQObj.offset().top - $(window).scrollTop());

}

interactionHelper.standardTouchEndEvent = function (jQObj, event, callback) {

    function shouldHandleTouchEnd (timeIntervalBetweenTouchStart, lastTouchElement, obj) {
        // Tap and hold Gesture
        // need to handle touch end if user did hold on a button and the touch end point is still the starting element.
        return (timeIntervalBetweenTouchStart > 400 && (isTheSameElement(lastTouchElement, obj) || isChildElementOfObject(lastTouchElement, obj)));
    }

    function isChildElementOfObject(child, parent) {
        var result =  ($(parent).has(child).length > 0)? true: false;
        return result;
    }

    function isTheSameElement(object1, object2) {
        var result =  ($(object1).get(0) === $(object2).get(0));
        return result;
    }

    // retrieve cached time from the object
    var startTime = jQObj.data('startTime');

    // get the last touch object on touch end
    var lastTouchElement = interactionHelper.currentTouchElementByEvent(event);

    var timeIntervalBetweenTouchStart = new Date() - startTime;

    if (startTime !== null ) {
        if(shouldHandleTouchEnd(timeIntervalBetweenTouchStart, lastTouchElement, jQObj) ) {
            
            // for fixing iOS mobile safari alert problem, we need to have timeout
            // when any alert is shown the touch end is never get fired
            setTimeout(function(){
                callback()
            }, 0);
        }
    }
}

interactionHelper.hasItemMovedWithEvent = function(jQObj, event, isSwiping, isScrolling) {

    var originalX = jQObj.data('originalX');
    var originalY = jQObj.data('originalY');
    var targetX = jQObj.offset().left - $(window).scrollLeft();
    var targetY = jQObj.offset().top - $(window).scrollTop();

    var diffX = targetX - originalX;
    var diffY = targetY - originalY;

    var didMove = (Math.abs(diffY) > 5 || Math.abs(diffX) > 5 );

    // if the webview on client support bouncing scrolling, the item offset will be still the same (when the reach the top/bottom)
    // so it is also necessary to check is the user is scrolling as well
    if (isScrolling) {
        didMove = true;
    }

    return didMove;
}

/*
* Helper method - get coordinate
* */
interactionHelper.getCoordinate = function (event, coordType) {
    var coordTypeString;

    switch (coordType) {
        case coordinateType.X :
            coordTypeString = 'pageX';
            break;
        case coordinateType.Y :
            coordTypeString = 'pageY';
            break;
        default : {
            return null;
        }
    }

    if (typeof(event.originalEvent) !== 'undefined' && event.originalEvent.changedTouches.length) {
        return /touch/.test(event.type) ? (event.originalEvent || event).changedTouches[0][coordTypeString] : event[coordTypeString];
    }
}

/*
 * Helper method - get current touching element
 * */
interactionHelper.currentTouchElementByEvent = function(event) {
    var touchPageX = interactionHelper.getCoordinate(event, coordinateType.X);
    var touchPageY = interactionHelper.getCoordinate(event, coordinateType.Y);
    var lastTouchElement = document.elementFromPoint(touchPageX - window.pageXOffset, touchPageY - window.pageYOffset);

    return lastTouchElement;
}


