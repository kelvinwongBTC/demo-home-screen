/*
* main class (the brain of the home screen)
*
* */

// jQuery
(function($){

    // on loading
    $(function() {
        layoutManager.setUpAppearance();
    });

    /*
    * For Debug use, this will returns the general error from the bridge API
    * */
    $('').btcjsapi({
        btcDebugger : function (data) {
            var json = BTCJSAPIHtmlManager.parseJsonData(data);

            if (json) {
                if(json.error) {
                    // do not alert the error unless you want user to know about
                    console.log('error occurred : ' + JSON.stringify(json.error));
                }
            }
        },
        shouldShowBTCLog:false // put true if  you want to see the request log
    });

})(jQuery);